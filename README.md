# Build Python!

Various scripts you can run to build Python from.

## Windows users

Don't touch the wallpaper on your way out.

## Command to execute

You should execute either the `wget` command, or the `curl` command, but not both.

### Python 3.8.3

```bash
wget -qO- https://gitlab.com/rock500/build-python/raw/master/Python-3.8.3 | sudo bash
curl -s https://gitlab.com/rock500/build-python/raw/master/Python-3.8.3 | sudo bash
```

### Python 3.7.2

```bash
wget -qO- https://gitlab.com/rock500/build-python/raw/master/Python-3.7.2 | sudo bash
curl -s https://gitlab.com/rock500/build-python/raw/master/Python-3.7.2 | sudo bash
```

### Python 3.7.1

```bash
wget -qO- https://gitlab.com/rock500/build-python/raw/master/Python-3.7.1 | sudo bash
curl -s https://gitlab.com/rock500/build-python/raw/master/Python-3.7.1 | sudo bash
```


### Python 3.7.0

```bash
wget -qO- https://gitlab.com/rock500/build-python/raw/master/Python-3.7.0 | sudo bash
curl -s https://gitlab.com/rock500/build-python/raw/master/Python-3.7.0 | sudo bash
```

### Python 3.6.6

```bash
wget -qO- https://gitlab.com/rock500/build-python/raw/master/Python-3.6.6 | sudo bash
curl -s https://gitlab.com/rock500/build-python/raw/master/Python-3.6.6 | sudo bash
```
